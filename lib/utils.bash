#!/usr/bin/env bash

set -euo pipefail

GH_REPO="https://github.com/apache/druid"
TOOL_NAME="druid"
TOOL_TEST="druid"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

list_github_tags() {
  git ls-remote --tags --refs "$GH_REPO" |
    grep -o 'refs/tags/.*' | 
    sed 's/^v//' | sed 's/^refs\/tags\/druid-//' | grep -wv -e "rc.*" -e "inc.*"
}

list_all_versions() {
  list_github_tags
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  rm -rf "$filename" &
  url="https://dlcdn.apache.org/druid/$version/apache-druid-$version-bin.tar.gz"
  echo "* Downloading $TOOL_NAME release $version..."
  echo "\"${curl_opts[@]}\" -o \"$filename\" -C - \"$url\""
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    touch "$install_path/bin/druid"
    echo "#!/bin/bash" >> "$install_path/bin/druid"
    echo "$install_path/bin/\${1}" >> "$install_path/bin/druid"
    chmod a+x "$install_path/bin/druid"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
